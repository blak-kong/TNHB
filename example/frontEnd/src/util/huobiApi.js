import * as RESTful from "@tnhb/api/dist/restful/index.js";
import { request } from "./http.js";
console.log("request", request);

/**
 * @function huobiRequest
 * @abstract 火币接口统一请求
 * @param title 请求接口名称
 * @param type asset basics market strategy transaction
 * @param method get post
 * @param params 请求参数
 * **/
async function huobiRequest(param) {
  const { title, type, method, params } = param;
  const restful = RESTful[type];
  var apiPath = null;
  Object.keys(restful[method]).forEach((item) => {
    if (restful[method][item].title == title) {
      apiPath = item;
    }
  });
  var res = await request(`/${type}?id=${apiPath}`, method, params);
  return res.data;
}

export default huobiRequest;
