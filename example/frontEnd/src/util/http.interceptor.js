import axios from "axios";

// 请求拦截器,可在请求前配置参数
axios.interceptors.request.use((config) => {
  // config.header.token = vm.vuex_token;
  // 方式一，存放在vuex的token，假设使用了uView封装的vuex方式，见：https://uviewui.com/components/globalVariable.html
  // config.header.token = vm.token;

  // 方式二，如果没有使用uView封装的vuex方法，那么需要使用$store.state获取
  // config.header.token = vm.$store.state.token;

  // 方式三，如果token放在了globalData，通过getApp().globalData获取
  // config.header.token = getApp().globalData.token;

  // 方式四，如果token放在了Storage本地存储中，拦截是每次请求都执行的，所以哪怕您重新登录修改了Storage，下一次的请求将会是最新值
  // const token = uni.getStorageSync('token');
  // config.header.token = token;

  return config;
});

// 响应拦截器，判断状态码是否通过
axios.interceptors.response.use(
  (res) => {
    // 如果把originalData设置为了true，这里得到将会是服务器返回的所有的原始数据
    // 判断可能变成了res.statueCode，或者res.data.code之类的，请打印查看结果
    // console.log("interceptor", res);
    return res;
  },
  (err) => {
    return Promise.reject(err);
  }
);

export default axios;
