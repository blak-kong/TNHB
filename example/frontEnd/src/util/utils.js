import dayjs from "dayjs";

/**
 * 获取未来一小时整点时间
 * @argument minute 设置超出整点分钟数
 * **/
export function getFutureHourTime(minute = 0) {
  let currentMinute = dayjs().minute();
  let futureMinute = 60 + minute;
  let add_minute = futureMinute - currentMinute;
  let futureTimestamp = dayjs().add(add_minute, "minute").valueOf();

  return Math.abs(dayjs().diff(futureTimestamp));
}

/**
 * 获取未来一分钟整点时间
 * @argument second 设置超出整点秒数
 * **/
export function getFutureMinTime(second = 0) {
  let currentSecond = dayjs().second();
  let futureSecond = 60 + second;
  let add_second = futureSecond - currentSecond;
  let futureTimestamp = dayjs().add(add_second, "second").valueOf();

  return Math.abs(dayjs().diff(futureTimestamp));
}

// uuid
export function uuid() {
  const UUID = "4b534c47-xxxx-4xxx-".replace(/[xy]/g, function (c) {
    var r = (Math.random() * 16) | 0,
      v = c == "x" ? r : (r & 0x3) | 0x8;
    return v.toString(16);
  });
  return UUID + Math.round(new Date().getTime() / 10);
}

export function getLocalStorage(key) {
  const value = localStorage.getItem(key);
  if (!value) return "";
  return JSON.parse(value);
}

export function setLocalStorage(key, value) {
  const stringify = JSON.stringify(value);
  localStorage.setItem(key, stringify);
}
