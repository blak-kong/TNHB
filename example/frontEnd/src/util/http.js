import axios from './http.interceptor.js'
import Cookie from 'js-cookie'

axios.defaults.baseURL = '' // 后端地址
if (process.env.NODE_ENV === 'development') {
  axios.defaults.baseURL = '/api'
}

axios.defaults.withCredentials = true

// http method
const METHOD = {
  GET: 'get',
  POST: 'post'
}

/**
 * axios请求
 * @param url 请求地址
 * @param method {METHOD} http method
 * @param params 请求参数
 * @returns {Promise<AxiosResponse<T>>}
 */
export async function request(url, method, params) {
  switch (method) {
    case METHOD.GET:
      return axios.get(url, { params })
    case METHOD.POST:
      return axios.post(url, params)
    default:
      return axios.get(url, { params })
  }
}

/**
 * axios 表单上传
 * @param url 请求地址
 * @param params 请求参数
 * @returns {Promise<AxiosResponse<T>>}
 */
export async function upload(url, params) {
  let http = axios.create({
    headers: {
      'content-type': 'multipart/form-data',
      token: Cookie.get('token')
    }
  })
  return http({
    method: METHOD.POST,
    url: url,
    data: params,
    processData: false,
    contentType: false
  })
}
