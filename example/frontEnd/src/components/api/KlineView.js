/** K线图 **/
import { dispose, init } from 'klinecharts'

import { ref, onMounted, onUnmounted } from 'vue'

export default function KLineChart() {
  //外部传入的参数
  onMounted(() => {
    InitChart()
  })
  onUnmounted(() => {
    DisposeChart()
  })

  const kLineChart = ref('')
  const PaneId = ref('')
  const InitChart = async () => {
    kLineChart.value = init('technical-indicator-k-line')
    kLineChart.value.setStyleOptions({
      grid: {
        // show: false
        horizontal: {
          color: '#222432'
        },
        vertical: {
          color: '#222432'
        }
      }
    })

    kLineChart.value.setPriceVolumePrecision(8, 8)

    PaneId.value = kLineChart.value.createTechnicalIndicator('MACD', false)
    // kLineChart.value.applyNewData();
  }
  const DisposeChart = function() {
    dispose('technical-indicator-k-line')
  }

  return {
    kLineChart,
    PaneId
  }
}
