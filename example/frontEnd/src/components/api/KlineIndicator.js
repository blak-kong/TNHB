/** 技术指标 **/

import { ref } from "vue";

export default function KlineIndicator(KlineView) {
  /**
   * @classdesc  蜡烛图指标（主图指标）
   * @params type<String> ['MA','EMA','SAR','BOLL']
   **/
  const setCandleTechnicalIndicator = async (type) => {
    KlineView.kLineChart.value.createTechnicalIndicator(type, false, {
      id: "candle_pane",
    });
  };

  /**
   * @classdesc 替代指标（副图指标）
   * @params type<String> ['VOL','MACD','KDJ','BOLL']
   **/
  const setSubTechnicalIndicator = async (type) => {
    KlineView.kLineChart.value.createTechnicalIndicator(type, false, {
      id: KlineView.PaneId.value,
    });
  };
  return {
    setCandleTechnicalIndicator,
    setSubTechnicalIndicator,
  };
}
