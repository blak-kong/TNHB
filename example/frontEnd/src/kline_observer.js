/*
 * K线：观察者模式
 * @desc  用于发布、订阅K线数据。但仅仅K线功能而言，有过度封装的感觉，并未实际使用
 */

// 定义 K线 发布者类
export class KPublisher {
  constructor() {
    this.observers = []
  }
  // 增加订阅者
  add(observer) {
    this.observers.push(observer)
  }
  // 移除订阅者
  remove(observer) {
    this.observers.forEach((item, i) => {
      if (item === observer) {
        this.observers.splice(i, 1)
      }
    })
  }
  // 通知所有订阅者
  notify() {
    this.observers.forEach(observer => {
      observer.update(this)
    })
  }
}

// 定义 K线 订阅者类
export class KObserver {
  constructor() {
    console.log('Observer created')
  }

  update() {
    console.log('Observer.update invoked')
  }
}

// 定义一个具体的发布类
export class KlinePublisher extends KPublisher {
  constructor() {
    super()
    // 初始化发布数据
    this.KlineState = null
    // 订阅 K线 最新数据的成员
    this.observers = []
  }

  // 该方法用于获取当前的 KlineState
  getState() {
    return this.KlineState
  }

  // 该方法用于改变 KlineState 的值
  setState(state) {
    // Kline 的值发生改变
    this.KlineState = state
    // K线变更，立刻通知所有订阅者
    this.notify()
  }
}

// 定义一个具体的订阅类
export class KlineObserver extends KObserver {
  constructor() {
    super()
    this.KlineState = {}
    this.Kline = null
  }

  // 重写一个具体的update方法
  update(publisher) {
    this.KlineState = publisher.getState() // 更新K线数据
    this.work() // 调用工作函数
  }

  // work方法，一个专门搬砖的方法
  work() {
    // K线最新数据
    const KlineData = this.KlineState

    console.log('K线最新数据', KlineData)
  }
}
