import { createApp } from 'vue'
import App from './App.vue'
// import ElementPlus from 'element-plus';
import { 
  ElButton,
  ElButtonGroup,
  ElTooltip,
  ElInfiniteScroll,
  ElLoading,
  ElMessage,
  ElMessageBox,
  ElNotification,
} from 'element-plus';
import 'element-plus/lib/theme-chalk/index.css';
// import JsonViewer from 'vue3-json-viewer';

import { request } from "./util/http.js";

const components = [
  ElInfiniteScroll,
  ElLoading,
  ElMessage,
  ElMessageBox,
  ElNotification,
  ElButton,
  ElButtonGroup,
  ElTooltip
]
const plugins = [
  ElInfiniteScroll,
  ElLoading,
  ElMessage,
  ElMessageBox,
  ElNotification,
]

const APP = createApp(App)

// Vue3 全局挂载方式更新
APP.config.globalProperties.$http = request;

components.forEach(component => {
  APP.component(component.name, component)
})

plugins.forEach(plugin => {
  APP.use(plugin)
})

// APP.use(JsonViewer)
// APP.use(ElementPlus, { size: 'small', zIndex: 3000 });
APP.mount('#app')

