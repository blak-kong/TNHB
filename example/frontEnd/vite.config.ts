import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import legacy from '@vitejs/plugin-legacy'
import { join } from "path"

// https://vitejs.dev/config/
export default defineConfig({
  base: './',
  build: {
    target: "esnext",
  },
  server: {
    port: 3000,
    proxy:{
      '/api': {
        // target: '后端不开源',
        target: 'http://localhost:7001',
        changeOrigin: true,
        rewrite: (path) => path.replace(/^\/api/, '')
      } 
    }
  },
  resolve: {
    alias: {
      '@src': join(__dirname, './src'),
      '@util': join(__dirname, './src/util'),
    },
  },
  plugins: [
    vue(),
    legacy({
      targets: ['ie >= 11'],
      additionalLegacyPolyfills: ['regenerator-runtime/runtime']
    })
  ], 
})
