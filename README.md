# TNHB

火币网永续 USDT 合约接口 SDK

前端开源 K 线获取，前端策略及后端不开源。

前端 K 线预览地址：http://hb.lzwlook.fun

## 下载安装

```javascript
// npm源
npm i tnhb -S
```

## 国内请求地址

```javascript
// 国内请求火币合约接口地址
api.btcgateway.pro // 国内默认使用此地址
api.hbdm.vn
```

## 后端配置文件

```javascript
npm i config -S
```

```javascript
// config/default.json
{
  "huobi": {
      "api_key": "xxxxxxxxxxxxx",
      "secret_key": "xxxxxxxxxxxxx",
      "url_prex": "https://api.btcgateway.pro",
      "ws_prex": "wss://api.btcgateway.pro",
      "host": "api.btcgateway.pro"
  }
}
```

```javascript
// {app_root}/dataConfig.js
const host = '' // 服务器地址
const pass = '' // 密码

module.exports = { host, pass }
```

```javascript
// {app_root}/app.js

const config = require('config')
const { AccessConfig, AccessConfigKit } = require('tnhb')

const apiConfig = new AccessConfig(config.huobi.api_key, config.huobi.secret_key)

AccessConfigKit.putAccessConfig(apiConfig)
AccessConfigKit.devMode = true

module.exports = app => {
  app.beforeStart(async () => {
    // 仅开发时开启数据库同步，避免出现生产事故
    // await app.model.sync({ force: false });
  })
}
```

## 使用方式

前端引入、调用示例：

```javascript
// Vite 不支持 Commonjs，需要项目使用 webpack 预构建，或者手动引入模块
import * as RESTful from '@tnhb/api/dist/restful/index.js'

/**
 * 调用参考:
 * example/frontEnd/util/http.js
 * example/frontEnd/util/huobiApi.js
 */
```

Eggjs 后端引入、调用示例（后端代码不开源）：

```javascript
'use strict'

const config = require('config')
const { HttpKit, Kits, RESTful } = require('tnhb')
const { asset } = RESTful
const Controller = require('egg').Controller

class assetController extends Controller {
  async index() {
    const { ctx } = this
    await ctx.render('index.html', {
      data: RESTful
    })
  }
  // GET
  async show() {
    const { ctx } = this
    const query = ctx.query
    const idname = ctx.params.id

    if (idname) {
      let path = asset.get[idname].path
      let res = await getHttp(path, query)
      ctx.body = res
    } else {
      ctx.body = { error: '接口不存在' }
    }
  }
  // POST
  async create() {
    const ctx = this.ctx
    const bodyParse = ctx.request.body
    const idname = ctx.query.id
    if (idname) {
      let path = asset.post[idname].path
      let res = await postHttp(path, bodyParse)
      ctx.body = res
    } else {
      ctx.body = { error: '接口不存在' }
    }
  }
}

async function getHttp(path, params = {}) {
  let dataSignature = Kits.get_sign_sha(path, params)
  let url = `${config.huobi.url_prex}${path}?${dataSignature}`
  return HttpKit.getHttpDelegate.httpGet(url, params)
}

async function postHttp(path, params = {}) {
  let dataSignature = Kits.post_sign_sha(path)
  let url = `${config.huobi.url_prex}${path}?${dataSignature}`
  return HttpKit.getHttpDelegate.httpPost(url, params)
}

module.exports = assetController
```

## HTTP 请求 火币 RESTful 接口（egg.js 封装后的统一一请求方式）

```javascript
/**
 * @Get请求
 * 请求格式：url/${router}/${api}?id=timestamp  (具体参数看火币官网文档，query参数需加多一个时间戳或uuid，防止Get请求缓存)
 * 请求示例：http://localhost:7001/asset/swap_api_trading_status?id=xxxxxx
 * **/
```

```javascript
/**
 * @Post请求
 * 请求格式：url/${router}?id=${api}
 * 请求示例：http://localhost:7001/strategy?id=swap_api_trading_status
 * **/
```
