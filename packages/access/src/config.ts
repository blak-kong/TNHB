/**
 * @author blak-kong
 * @copyright blak-kong@foxmail.com
 * @description ApiConfig
 */

 export class AccessConfig {
  private accessKey: string
  private accessScrect: string

  constructor(accessKey?: string, accessScrect?: string) {
    this.accessKey = accessKey
    this.accessScrect = accessScrect
  }

  public get getaccessKey(): string {
    return this.accessKey
  }

  public set setaccessKey(accessKey: string) {
    this.accessKey = accessKey
  }

  public get getaccessScrect(): string {
    return this.accessScrect
  }

  public set setaccessScrect(accessScrect: string) {
    this.accessScrect = accessScrect
  }
}
