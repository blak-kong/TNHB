import { AccessConfig } from './config'
import { ICache, DefaultCache } from '@tnhb/cache'

export class AccessConfigKit {
  static CFG_MAP: Map<String, AccessConfig> = new Map<String, AccessConfig>()

  static currentaccessKey: string

  static DEFAULT_CFG_KEY: string = '_default_cfg_key_'

  static devMode: boolean = false

  static _cache: ICache = new DefaultCache()

  public set devMode(devMode: boolean) {
    this.devMode = devMode
  }
  public static isDevMode(): boolean {
    return this.devMode
  }

  public static putAccessConfig(accessConfig: AccessConfig) {
    if (this.CFG_MAP.size == 0) {
      this.CFG_MAP.set(this.DEFAULT_CFG_KEY, accessConfig)
    }
    return this.CFG_MAP.set(accessConfig.getaccessKey, accessConfig)
  }

  public static removeAccessConfigByConfig(accessConfig: AccessConfig): boolean {
    return this.removeAccessConfig(accessConfig.getaccessKey)
  }

  public static removeAccessConfig(accessKey: string): boolean {
    return this.CFG_MAP.delete(accessKey)
  }

  public static setCurrentaccessKey(accessKey?: string) {
    if (accessKey) {
      this.currentaccessKey = accessKey
    } else {
      let accessConfig = this.CFG_MAP.get(this.DEFAULT_CFG_KEY)
      if (accessConfig) {
        accessKey = accessConfig.getaccessKey
        this.currentaccessKey = accessKey
      }
    }
  }

  public static removeCurrentaccessKey() {
    this.currentaccessKey = ''
  }

  public static get getaccessKey(): string {
    let accessKey: string = this.currentaccessKey
    if (!accessKey) {
      let accessConfig = this.CFG_MAP.get(this.DEFAULT_CFG_KEY)
      if (accessConfig) {
        accessKey = accessConfig.getaccessKey
      }
    }
    return accessKey
  }


  public static get getAccessConfig(): AccessConfig {
    return this.getAccessConfigByaccessKey(this.getaccessKey)
  }

  public static getAccessConfigByaccessKey(accessKey: string): AccessConfig {
    if (AccessConfigKit.isDevMode()) {
      console.debug(`当前为开发模式 getAccessConfigByaccessKey accessKey: ${accessKey}`)
    }
    let cfg = this.CFG_MAP.get(accessKey)
    if (!cfg) {
      throw new Error('需事先调用 AccessConfigKit.putAccessConfig(AccessConfig) 将 accessKey 对应的 AccessConfig 对象存入后,才可以使用 AccessConfigKit.getAccessConfig 系列方法')
    }
    return cfg
  }

  public static get getCache(): ICache {
    return this._cache
  }

  public static set setCache(cache: ICache) {
    this._cache = cache
  }
}
