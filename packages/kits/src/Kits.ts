/**
 * @author blak-kong
 * @copyright blak-kong@foxmail.com
 * @description 常用的工具方法
 */
import * as CryptoJS from 'crypto-js'
import * as uuid from 'uuid'
import { AccessConfigKit } from "@tnhb/access"


export class Kits {

   /**
   * sha 加密 urlParams ,生成数字签名
   * @param access_key 用户key
   * @param secret_key 密钥key
   * @param method 请求类型
   * @param baseurl 请求域名 host
   * @param path    请求地址
   * @param params    加密数据
   */
 public static get_sign_sha(path:string, params:any, host:string): any {
    const apiHost = host || "api.btcgateway.pro";
    const apiParams = params || {};
    return this.sign_sha("GET", apiHost, path, apiParams);
  }

  public static post_sign_sha(path:string, params:any, host:string): any {
    const apiHost = host || "api.btcgateway.pro";
    const apiParams = params || {};
    return this.sign_sha("POST", apiHost, path, apiParams); 
  }

  public static sign_sha(method:string, baseurl:string, path:string, data:any): any {
    
    let accessConfig = AccessConfigKit.getAccessConfig;
    let rawParams = this.getUrlRawParams(accessConfig);
    let paramsData = Object.assign(rawParams, data);

    let paramsArray = [];
    for (let item in paramsData) {
      if (paramsData.hasOwnProperty(item)) {
        paramsArray.push(item + "=" + encodeURIComponent(paramsData[item]));
      }
    }
    paramsArray.sort(); // 排序会在原数组上操作，不生成副本
    
    let urlParams = paramsArray.join("&");
    const meta = [method, baseurl, path, urlParams].join("\n");
    const hash = CryptoJS.HmacSHA256(meta, accessConfig.getaccessScrect);
    const Signature = encodeURIComponent(CryptoJS.enc.Base64.stringify(hash));

    urlParams += `&Signature=${Signature}`;   
    return urlParams;
  }


 public static getUrlRawParams(accessConfig: any): any {
    return {
      AccessKeyId: accessConfig.getaccessKey,
      SignatureMethod: "HmacSHA256",
      SignatureVersion: 2,
      Timestamp: new Date().toISOString().slice(0, 19) // 使用 ISO 标准返回 Date 对象的字符串格式
    };
 }


  /**
   * 随机生成字符串
   */
  public static generateStr(): string {
    return uuid.v4().replace(/\-/g, '')
  }
}
