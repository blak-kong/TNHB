/**
 * @author blak-kong
 * @copyright blak-kong@foxmail.com
 * @description access 缓存接口
 */

export interface ICache {
  get(key: string): Promise<any>
  set(key: string, jsonValue: string): Promise<any>
  remove(key: string): Promise<any>
}
