/* 基础信息接口 GET */
import { apiPrefix } from "./prefix";
export const basics = {
  get: {
    swap_contract_info: {
      path: `${apiPrefix}/swap_contract_info`,
      title: "【通用】获取合约信息"
    },
    swap_index: {
      path: `${apiPrefix}/swap_index`,
      title: "【通用】获取合约指数信息"
    },
    swap_price_limit: {
      path: `${apiPrefix}/swap_price_limit`,
      title: "【通用】获取合约最高限价和最低限价"
    },
    swap_open_interest: {
      path: `${apiPrefix}/swap_open_interest`,
      title: "【通用】获取当前可用合约总持仓量"
    },
    swap_risk_info: {
      path: `${apiPrefix}/swap_risk_info`,
      title: "【通用】查询合约风险准备金和预估分摊比例"
    },
    swap_insurance_fund: {
      path: `${apiPrefix}/swap_insurance_fund`,
      title: "【通用】获取风险准备金历史数据"
    },

    swap_adjustfactor: {
      path: `${apiPrefix}/swap_adjustfactor`,
      title: "【逐仓】查询平台阶梯调整系数"
    },
    swap_cross_adjustfactor: {
      path: `${apiPrefix}/swap_cross_adjustfactor`,
      title: "【全仓】查询平台阶梯调整系数"
    },

    swap_his_open_interest: {
      path: `${apiPrefix}/swap_his_open_interest`,
      title: "【逐仓】获取平台持仓量"
    },
    swap_elite_account_ratio: {
      path: `${apiPrefix}/swap_elite_account_ratio`,
      title: "【通用】精英账户多空持仓对比-账户数"
    },
    swap_elite_position_ratio: {
      path: `${apiPrefix}/swap_elite_position_ratio`,
      title: "【通用】精英账户多空持仓对比-持仓量"
    },
    swap_liquidation_orders: {
      path: `${apiPrefix}/swap_liquidation_orders`,
      title: "【通用】获取强平订单"
    },
    swap_settlement_records: {
      path: `${apiPrefix}/swap_settlement_records`,
      title: "【通用】平台历史结算记录"
    },

    swap_cross_transfer_state: {
      path: `${apiPrefix}/swap_cross_transfer_state`,
      title: "【全仓】查询系统划转权限"
    },
    swap_cross_trade_state: {
      path: `${apiPrefix}/swap_cross_trade_state`,
      title: "【全仓】查询系统交易权限"
    },
    swap_api_state: {
      path: `${apiPrefix}/swap_api_state`,
      title: "【逐仓】查询系统状态"
    },
  },
  post: {}
}

module.exports = basics;