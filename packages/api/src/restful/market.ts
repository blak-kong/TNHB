/* 市场行情接口 */
import { apiPrefix } from "./prefix";
const market = {
  get: {
    swap_funding_rate: {
      path: `${apiPrefix}/swap_funding_rate`,
      title: "【通用】获取合约资金费率"
    },
    swap_batch_funding_rate: {
      path: `${apiPrefix}/swap_batch_funding_rate`,
      title: "【通用】批量获取合约资金费率"
    },
    swap_historical_funding_rate: {
      path: `${apiPrefix}/swap_historical_funding_rate`,
      title: "【通用】获取合约的历史资金费率"
    },
    swap_ladder_margin: {
      path: `${apiPrefix}/swap_ladder_margin`,
      title: "【逐仓】获取平台阶梯保证金"
    },
    swap_cross_ladder_margin: {
      path: `${apiPrefix}/swap_cross_ladder_margin`,
      title: "【全仓】获取平台阶梯保证金"
    },
    swap_estimated_settlement_price: {
      path: `${apiPrefix}/swap_estimated_settlement_price`,
      title: "【通用】获取预估结算价"
    },

    linear_swap_basis: {
      path: `/index/market/history/linear_swap_basis`,
      title: "【通用】获取基差数据"
    },
    linear_swap_mark_price_kline: {
      path: `/index/market/history/linear_swap_mark_price_kline`,
      title: "【通用】获取溢价指数K线数据"
    },
    linear_swap_estimated_rate_kline: {
      path: `/index/market/history/linear_swap_estimated_rate_kline`,
      title: "【通用】获取预测资金费率的K线数据"
    },
    depth: {
      path: `/linear-swap-ex/market/depth`,
      title: "【通用】获取行情深度数据"
    },
    bbo: {
      path: `/linear-swap-ex/market/bbo`,
      title: "【通用】获取市场最优挂单"
    },
    kline: {
      path: `/linear-swap-ex/market/history/kline`,
      title: "【通用】获取K线数据"
    },
    merged: {
      path: `/linear-swap-ex/market/detail/merged`,
      title: "【通用】获取聚合行情"
    },
    batch_merged: {
      path: `/linear-swap-ex/market/detail/batch_merged`,
      title: "【通用】批量获取聚合行情"
    },
    trade: {
      path: `/linear-swap-ex/market/trade`,
      title: "【通用】获取市场最近成交记录"
    },
    batch_trade: {
      path: `/linear-swap-ex/market/history/trade`,
      title: "【通用】批量获取最近的交易记录"
    },
  },
  post: {}
}

module.exports = market;