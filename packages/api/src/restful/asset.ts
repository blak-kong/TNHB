/* 账户资产接口 */
import { apiPrefix } from "./prefix";
const asset = {
  get: {
    swap_api_trading_status: {
      path: `${apiPrefix}/swap_api_trading_status`,
      title: "【通用】获取用户API指标禁用信息"
    },
  },
  post: { 
    swap_balance_valuation: {
      path: `${apiPrefix}/swap_balance_valuation`,
      title: "获取账户总资产估值"
    },
    swap_account_info: {
      path: `${apiPrefix}/swap_account_info`,
      title: "【逐仓】获取用户的合约账户信息"
    },
  
    swap_position_info: {
      path: `${apiPrefix}/swap_position_info`,
      title: "【逐仓】获取用户的合约持仓信息"
    },
    swap_available_level_rate: {
      path: `${apiPrefix}/swap_available_level_rate`,
      title: "【逐仓】查询用户可用杠杆倍数"
    },
  
  
    // 子账户相关
    swap_sub_auth: {
      path: `${apiPrefix}/swap_sub_auth`,
      title: "【通用】批量设置子账户交易权限"
    },
    swap_sub_account_list: {
      path: `${apiPrefix}/swap_sub_account_list`,
      title: "【逐仓】查询母账户下所有子账户资产信息"
    },
    swap_sub_account_info_list: {
      path: `${apiPrefix}/swap_sub_account_info_list`,
      title: "【逐仓】批量获取子账户资产信息"
    }, 
    swap_cross_sub_account_info_list: {
      path: `${apiPrefix}/swap_cross_sub_account_info_list`,
      title: "【全仓】批量获取子账户资产信息"
    },
    swap_sub_account_info: {
      path: `${apiPrefix}/swap_sub_account_info`,
      title: "【逐仓】查询母账户下的单个子账户资产信息"
    },
    swap_sub_position_info: {
      path: `${apiPrefix}/swap_sub_position_info`,
      title: "【逐仓】查询母账户下的单个子账户持仓信息"
    },
  
    // 财务
    swap_financial_record: {
      path: `${apiPrefix}/swap_financial_record`,
      title: "【通用】查询用户财务记录"
    },
    swap_financial_record_exact: {
      path: `${apiPrefix}/swap_financial_record_exact`,
      title: "【通用】组合查询用户财务记录"
    },
    // 结算
    swap_user_settlement_records: {
      path: `${apiPrefix}/swap_financial_record_exact`,
      title: "【逐仓】查询用户结算记录"
    },
    swap_cross_user_settlement_records: {
      path: `${apiPrefix}/swap_cross_user_settlement_records`,
      title: "【全仓】查询用户结算记录"
    },
    
    // 限制和费率
    swap_order_limit: {
      path: `${apiPrefix}/swap_order_limit`,
      title: "【通用】获取用户的合约下单量限制"
    },
    swap_fee: {
      path: `${apiPrefix}/swap_fee`,
      title: "【通用】获取用户的合约手续费费率"
    },
    swap_transfer_limit: {
      path: `${apiPrefix}/swap_order_limit`,
      title: "【逐仓】获取用户的合约划转限制"
    },
    swap_position_limit: {
      path: `${apiPrefix}/swap_order_limit`,
      title: "【逐仓】获取用户的合约持仓量限制"
    },
    swap_account_position_info: {
      path: `${apiPrefix}/swap_account_position_info`,
      title: "【逐仓】获取用户资产和持仓信息"
    },
  
    // 账户划转
    swap_master_sub_transfer: {
      path: `${apiPrefix}/swap_master_sub_transfer`,
      title: "【通用】母子账户划转"
    },
    swap_master_sub_transfer_record: {
      path: `${apiPrefix}/swap_master_sub_transfer_record`,
      title: "【通用】母子账户划转记录"
    },
    swap_transfer_inner: {
      path: `${apiPrefix}/swap_transfer_inner`,
      title: "【通用】同账号不同保证金账户的划转"
    },

    swap_cross_account_info: {
      path: `${apiPrefix}/swap_cross_account_info`,
      title: "【全仓】获取用户的合约 账户信息"
    },
    swap_cross_position_info: {
      path: `${apiPrefix}/swap_cross_position_info`,
      title: "【全仓】获取用户的合约 持仓信息"
    },


    swap_cross_sub_account_list: {
      path: `${apiPrefix}/swap_cross_sub_account_list`,
      title: "【全仓】查询母账户下所有子账户资产信息"
    },
    swap_cross_sub_account_info: {
      path: `${apiPrefix}/swap_cross_sub_account_info`,
      title: "【全仓】查询母账户下的单个子账户资产信息"
    },

    swap_cross_sub_position_info: {
      path: `${apiPrefix}/swap_cross_sub_position_info`,
      title: "【全仓】查询母账户下的单个子账户持仓信息"
    },
    swap_cross_transfer_limit: {
      path: `${apiPrefix}/swap_cross_transfer_limit`,
      title: "【全仓】获取用户的合约划转限制"
    },
    swap_cross_position_limit: {
      path: `${apiPrefix}/swap_cross_position_limit`,
      title: "【全仓】获取用户的合约持仓量限制"
    },
    swap_cross_account_position_info: {
      path: `${apiPrefix}/swap_cross_account_position_info`,
      title: "【全仓】获取用户资产和持仓信息"
    },
    swap_cross_available_level_rate: {
      path: `${apiPrefix}/swap_cross_available_level_rate`,
      title: "【全仓】获取用户当前合约杠杆倍数"
    },
  },
}

module.exports = asset;