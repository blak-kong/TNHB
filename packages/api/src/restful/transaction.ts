/* 交易接口 */
import { apiPrefix } from "./prefix";
const transaction = {
  get: {},
  post: {
    transfer: {
      path: `/v2/account/transfer`,
      title: "【通用】现货-USDT本位永续账户间进行资金的划转"
    },
    swap_order: {
      path: `${apiPrefix}/swap_order`,
      title: "【逐仓】合约下单"
    },
    swap_batchorder: {
      path: `${apiPrefix}/swap_batchorder`,
      title: "【逐仓】合约批量下单"
    },
    swap_switch_lever_rate: {
      path: `${apiPrefix}/swap_switch_lever_rate`,
      title: "【逐仓】切换杠杆"
    },
    swap_cancel: {
      path: `${apiPrefix}/swap_cancel`,
      title: "【逐仓】撤销合约订单"
    },
    swap_cancelall: {
      path: `${apiPrefix}/swap_cancelall`,
      title: "【逐仓】撤销全部合约单"
    },
    swap_order_info: {
      path: `${apiPrefix}/swap_order_info`,
      title: "【逐仓】获取用户的合约订单信息"
    },
    swap_order_detail: {
      path: `${apiPrefix}/swap_order_detail`,
      title: "【逐仓】获取用户的合约订单明细信息"
    },
    swap_openorders: {
      path: `${apiPrefix}/swap_openorders`,
      title: "【逐仓】获取用户的合约当前未成交委托"
    },
    swap_hisorders: {
      path: `${apiPrefix}/swap_hisorders`,
      title: "【逐仓】获取用户的合约历史委托"
    },
    swap_hisorders_exact: {
      path: `${apiPrefix}/swap_hisorders_exact`,
      title: "【逐仓】组合查询合约历史委托"
    },
    swap_matchresults: {
      path: `${apiPrefix}/swap_matchresults`,
      title: "【逐仓】获取用户的合约历史成交记录"
    },
    swap_matchresults_exact: {
      path: `${apiPrefix}/swap_matchresults_exact`,
      title: "【逐仓】组合查询用户历史成交记录"
    },
    swap_lightning_close_position: {
      path: `${apiPrefix}/swap_lightning_close_position`,
      title: "【逐仓】合约闪电平仓下单"
    },
    
    swap_cross_switch_lever_rate: {
      path: `${apiPrefix}/swap_cross_switch_lever_rate`,
      title: "【全仓】切换杠杆"
    },
    swap_cross_order: {
      path: `${apiPrefix}/swap_cross_order`,
      title: "【全仓】合约下单"
    },
    swap_cross_batchorder: {
      path: `${apiPrefix}/swap_cross_batchorder`,
      title: "【全仓】合约批量下单"
    },
    swap_cross_cancel: {
      path: `${apiPrefix}/swap_cross_cancel`,
      title: "【全仓】撤销合约订单"
    },
    swap_cross_cancelall: {
      path: `${apiPrefix}/swap_cross_cancelall`,
      title: "【全仓】撤销全部合约单"
    },
    swap_cross_trigger_order: {
      path: `${apiPrefix}/swap_cross_trigger_order`,
      title: "【全仓】合约计划委托下单"
    },

    /* 读取 */
    swap_cross_order_info: {
      path: `${apiPrefix}/swap_cross_order_info`,
      title: "【全仓】获取用户的合约订单信息"
    },
    swap_cross_order_detail: {
      path: `${apiPrefix}/swap_cross_order_detail`,
      title: "【全仓】获取用户的合约订单明细信息"
    },
    swap_cross_openorders: {
      path: `${apiPrefix}/swap_cross_openorders`,
      title: "【全仓】获取用户的合约当前未成交委托"
    },
    swap_cross_hisorders: {
      path: `${apiPrefix}/swap_cross_hisorders`,
      title: "【全仓】获取用户的合约历史委托"
    },
    swap_cross_hisorders_exact: {
      path: `${apiPrefix}/swap_cross_hisorders_exact`,
      title: "【全仓】组合查询合约历史委托"
    },
    swap_cross_matchresults: {
      path: `${apiPrefix}/swap_cross_matchresults`,
      title: "【全仓】获取用户的合约历史成交记录"
    },
    swap_cross_matchresults_exact: {
      path: `${apiPrefix}/swap_cross_matchresults_exact`,
      title: "【全仓】组合查询用户历史成交记录"
    },
    swap_cross_lightning_close_position: {
      path: `${apiPrefix}/swap_cross_lightning_close_position`,
      title: "【全仓】合约闪电平仓下单"
    },
   
  }
}
module.exports = transaction;