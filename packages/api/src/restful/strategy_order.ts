/* 策略订单/计划订单 接口 */

import { apiPrefix } from "./prefix";
const strategy_order = {
  get: {},
  post: {
    swap_trigger_order: {
      path: `${apiPrefix}/swap_trigger_order`,
      title: "【逐仓】合约计划委托下单"
    },
    swap_trigger_cancel: {
      path: `${apiPrefix}/swap_trigger_cancel`,
      title: "【逐仓】合约计划委托撤单"
    },
    swap_trigger_cancelall: {
      path: `${apiPrefix}/swap_trigger_cancelall`,
      title: "【逐仓】合约计划委托全部撤单"
    },
    swap_trigger_openorders: {
      path: `${apiPrefix}/swap_trigger_openorders`,
      title: "【逐仓】获取计划委托当前委托"
    },
    swap_trigger_hisorders: {
      path: `${apiPrefix}/swap_trigger_hisorders`,
      title: "【逐仓】获取计划委托历史委托"
    },

    swap_cross_trigger_order: {
      path: `${apiPrefix}/swap_cross_trigger_order`,
      title: "【全仓】合约计划委托下单"
    },
    swap_cross_trigger_cancel: {
      path: `${apiPrefix}/swap_cross_trigger_cancel`,
      title: "【全仓】合约计划委托撤单"
    },
    swap_cross_trigger_cancelall: {
      path: `${apiPrefix}/swap_cross_trigger_cancelall`,
      title: "【全仓】合约计划委托全部撤单"
    },
    swap_cross_trigger_openorders: {
      path: `${apiPrefix}/swap_cross_trigger_openorders`,
      title: "【全仓】获取计划委托当前委托"
    },
    swap_cross_trigger_hisorders: {
      path: `${apiPrefix}/swap_cross_trigger_hisorders`,
      title: "【全仓】获取计划委托历史委托"
    },

    swap_tpsl_order: {
      path: `${apiPrefix}/swap_tpsl_order`,
      title: "【逐仓】对仓位设置止盈止损订单"
    },
    swap_tpsl_cancel: {
      path: `${apiPrefix}/swap_tpsl_cancel`,
      title: "【逐仓】止盈止损订单撤单"
    },
    swap_tpsl_cancelall: {
      path: `${apiPrefix}/swap_tpsl_cancelall`,
      title: "【逐仓】止盈止损订单全部撤单"
    },
    swap_tpsl_openorders: {
      path: `${apiPrefix}/swap_tpsl_openorders`,
      title: "【逐仓】止盈止损订单当前委托"
    },
    swap_tpsl_hisorders: {
      path: `${apiPrefix}/swap_tpsl_hisorders`,
      title: "【逐仓】止盈止损订单历史委托"
    },
    swap_relation_tpsl_order: {
      path: `${apiPrefix}/swap_relation_tpsl_order`,
      title: "【逐仓】查询开仓单关联的止盈止损订单"
    },

    swap_cross_tpsl_order: {
      path: `${apiPrefix}/swap_cross_tpsl_order`,
      title: "【全仓】对仓位设置止盈止损订单"
    },
    swap_cross_tpsl_cancel: {
      path: `${apiPrefix}/swap_cross_tpsl_cancel`,
      title: "【全仓】止盈止损订单撤单"
    },
    swap_cross_tpsl_cancelall: {
      path: `${apiPrefix}/swap_cross_tpsl_cancelall`,
      title: "【全仓】止盈止损订单全部撤单"
    },
    swap_cross_tpsl_openorders: {
      path: `${apiPrefix}/swap_cross_tpsl_openorders`,
      title: "【全仓】止盈止损订单当前委托"
    },
    swap_cross_tpsl_hisorders: {
      path: `${apiPrefix}/swap_cross_tpsl_hisorders`,
      title: "【全仓】止盈止损订单历史委托"
    },
    swap_cross_relation_tpsl_order: {
      path: `${apiPrefix}/swap_cross_relation_tpsl_order`,
      title: "【全仓】查询开仓单关联的止盈止损订单"
    },

    swap_track_order: {
      path: `${apiPrefix}/swap_track_order`,
      title: "【逐仓】跟踪委托订单下单"
    },
    swap_track_cancel: {
      path: `${apiPrefix}/swap_track_cancel`,
      title: "【逐仓】跟踪委托订单撤单"
    },
    swap_track_cancelall: {
      path: `${apiPrefix}/swap_track_cancelall`,
      title: "【逐仓】跟踪委托订单全部撤单"
    },
    swap_track_openorders: {
      path: `${apiPrefix}/swap_track_openorders`,
      title: "【逐仓】跟踪委托订单当前委托"
    },
    swap_track_hisorders: {
      path: `${apiPrefix}/swap_track_hisorders`,
      title: "【逐仓】跟踪委托订单历史委托"
    },

    swap_cross_track_order: {
      path: `${apiPrefix}/swap_cross_track_order`,
      title: "【全仓】跟踪委托订单下单"
    },
    swap_cross_track_cancel: {
      path: `${apiPrefix}/swap_cross_track_cancel`,
      title: "【全仓】跟踪委托订单撤单"
    },
    swap_cross_track_cancelall: {
      path: `${apiPrefix}/swap_cross_track_cancelall`,
      title: "【全仓】跟踪委托订单全部撤单"
    },
    swap_cross_track_openorders: {
      path: `${apiPrefix}/swap_cross_track_openorders`,
      title: "【全仓】跟踪委托订单当前委托"
    },
    swap_cross_track_hisorders: {
      path: `${apiPrefix}/swap_cross_track_hisorders`,
      title: "【全仓】跟踪委托订单历史委托"
    },
  }
}
module.exports = strategy_order;