/* 
  linear-swap-ws           市场行情 请求以及订阅地址
  ws_index                 指数K线及基差数据 订阅地址
  linear-swap-notification 订单推送订阅
*/

export const wsHuobi = {
  sub: {
    "订阅KLine数据": {
      path: "market.$contract_code.kline.$period",
      title: "【通用】订阅KLine数据"
    },
    "订阅MarketDepth数据": {
      path: "market.$contract_code.depth.$type",
      title: "【通用】订阅 Market Depth 数据"
    },
    "订阅MarketDepth增量推送数据": {
      path: "market.$contract_code.depth.size_${size}.high_freq",
      title: "【通用】订阅 Market Depth增量推送数据"
    },
    "买一卖一逐笔行情推送": {
      path: "market.$contract_code.bbo",
      title: "【通用】买一卖一逐笔行情推送"
    },
    "订阅MarketDetail数据": {
      path: "market.$contract_code.detail",
      title: "【通用】订阅MarketDetail数据"
    },
    "订阅TradeDetail数据": {
      path: "	market.$contract_code.trade.detail",
      title: "【通用】订阅TradeDetail数据"
    },
    /* K线、行情订阅 linear-swap-ws */


    "订阅指数K线数据": {
      path: "market.$contract_code.index.$period",
      title: "【通用】订阅指数K线数据",
    },
    "订阅基差数据": {
      path: "market.$contract_code.basis.$period.$basis_price_type",
      title: "【通用】订阅基差数据"
    },
    "订阅溢价指数K线数据": {
      path: "market.$contract_code.premium_index.$period",
      title: "【通用】订阅溢价指数K线数据"
    },
    "订阅预测资金费率K线数据": {
      path: "market.$contract_code.estimated_rate.$period",
      title: "【通用】订阅预测资金费率K线数据"
    },
    "订阅标记价格K线数据": {
      path: "market.$contract_code.mark_price.$period",
      title: "【通用】订阅标记价格K线数据"
    },
   
    /* 指数与基差接口 ws_index */

    "订阅强平订单数据": {
      path: "public.$contract_code.liquidation_orders",
      title: "【通用】订阅强平订单数据（免鉴权）"
    },
    "订阅资金费率变动数据": {
      path: "public.$contract_code.funding_rate",
      title: "【通用】订阅资金费率变动数据（免鉴权）"
    },
    "订阅合约信息变动数据": {
      path: "public.$contract_code.contract_info",
      title: "【通用】订阅合约信息变动数据（免鉴权）"
    },
    

    /* 以下需要鉴权 */
    "逐仓订阅资产变动数据": {
      path: "accounts.$contract_code",
      title: "【逐仓】订阅资产变动数据"
    },
    "全仓订阅资产变动数据": {
      path: "accounts_cross.$margin_account",
      title: "【全仓】订阅资产变动数据"
    },
    /**/

    "逐仓订阅持仓变动更新数据": {
      path: "positions.$contract_code",
      title: "【逐仓】订阅持仓变动更新数据"
    },
    /**/
    
    "逐仓订阅订单成交数据": {
      path: "orders.$contract_code",
      title: "【逐仓】订阅订单成交数据"
    },
    "全仓订阅订单成交数据": {
      path: "orders_cross.$contract_code",
      title: "【全仓】订阅订单成交数据"
    },
    /**/


    "逐仓订阅撮合订单成交数据": {
      path: "matchOrders.$contract_code",
      title: "【逐仓】订阅撮合订单成交数据"
    },
    "全仓订阅撮合订单成交数据": {
      path: "matchOrders_cross.$contract_code",
      title: "【全仓】订阅撮合订单成交数据"
    },
    /**/

    "全仓订阅计划委托订单变动": {
      path: "trigger_order_cross.$contract_code",
      title: "【全仓】订阅计划委托订单变动"
    },
  },
  req: {
    /* 全部不需要验签 */
    "请求KLine数据": {
      path: "market.$contract_code.kline.$period",
      title: "【通用】请求KLine数据"
    },
    "请求TradeDetail数据": {
      path: "market.$contract_code.trade.detail",
      title: "【通用】请求TradeDetail数据"
    },
    "请求指数K线数据": {
      path: "market.$contract_code.index.$period",
      title: "【通用】请求指数K线数据"
    },
    "请求基差数据": {
      path: "market.$contract_code.basis.$period.$basis_price_type",
      title: "【通用】请求基差数据"
    },
    "请求溢价指数K线数据": {
      path: "market.$contract_code.premium_index.$period",
      title: "【通用】请求溢价指数K线数据"
    },
    "请求预测资金费率K线数据": {
      path: "market.$contract_code.estimated_rate.$period",
      title: "【通用】请求预测资金费率K线数据"
    },
    "请求标记价格K线数据": {
      path: "market.$contract_code.mark_price.$period",
      title: "【通用】请求标记价格K线数据"
    },
  }
}
